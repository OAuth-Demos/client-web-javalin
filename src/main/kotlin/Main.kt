package com.bve

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelManager
import com.github.kittinunf.fuel.core.HttpException
import io.javalin.Context
import io.javalin.Javalin
import org.slf4j.LoggerFactory
import javax.ws.rs.core.UriBuilder


object  AuthZServer {
    // EndPoints
    const val TOKEN_URL = "http://localhost:8080/auth/realms/demo/protocol/openid-connect/token"
    const val AUTHZ_URL = "http://localhost:8080/auth/realms/demo/protocol/openid-connect/auth"

    // Client Credentials
    const val CLIENT_ID = "client-web-javalin"
    const val CLIENT_SECRET = "8c911a06-909b-41c2-9fd0-b61fce39dc8f"

    const val REDIRECT_URL = "http://localhost:7000/callback"
}

const val RESOURCE_SERVER = "http://localhost:9090"     // My Resource Server "rs-web-javalin"


/**
 * Check the doc: https://javalin.io/documentation
 */

fun main(args: Array<String>) {
    val logger = LoggerFactory.getLogger("Main")

    val app = Javalin.create().apply {
        //enableStandardRequestLogging()
        enableStaticFiles("Semantic-UI-CSS-master")
        port(7000)
    }.start()

    fun getModel(ctx: Context, msg: String=""): Map<String, Any?> = mapOf(
            "path" to ctx.path(),
            "msg" to msg ,
            "session" to  ctx.sessionAttribute<AccessTokenResponse?>("Token")
    )

    app.get("/")      { ctx -> ctx.render("prez.html", getModel(ctx)) }
    app.get("/base")  { ctx -> ctx.render("prez.html", getModel(ctx)) }
    app.get("/redir") { ctx -> ctx.render("prez.html", getModel(ctx)) }

    app.get("/reset") { ctx ->
        ctx.req.session.invalidate()
        ctx.render("prez.html", getModel(ctx,"Session Invalidated (=destroyed, reset ...)"))
    }


    // First: Request a Token
    app.get("/reqtok") {

        // I am already connected ?
        if (it.sessionAttribute<AccessTokenResponse>("Token") != null) {    // Yes! It can be null !!!
            // We could check if it has expired ...
            logger.debug("We already have an Access Token in the Session.")
            it.redirect("/delegaccess#already-have-token")
        } else {
            // If not redirect to the Authorization Server
            val authServ = UriBuilder.fromUri(AuthZServer.AUTHZ_URL)
                    .queryParam("response_type", "code")
                    .queryParam("client_id", AuthZServer.CLIENT_ID)
                    .queryParam("scope", "role-monitor")
                    .queryParam("state", "NonceThatShouldBeChecked")
                    .queryParam("redirect_uri", AuthZServer.REDIRECT_URL)
                    .build()

            logger.debug("Redirect to : " + authServ.toASCIIString())
            it.redirect(authServ.toASCIIString(), 302)
        }
    }


    app.get("/callback") {
        // This URL is not meant to be requested directly
        // the Authorization Server redirects here with an Authorization Code
        // This Code will allow us to query the Access/Refresh and ID Tokens
        val authorizationCode = it.queryParam("code")
        logger.debug("Got  authorizationCode: $authorizationCode")

        // Fuel query     https://tools.ietf.org/html/rfc6749#section-4.1.3
        try {

        val (request, response, result) = Fuel
                .post(AuthZServer.TOKEN_URL,
                     listOf(
                        "grant_type" to "authorization_code",
                        "code" to authorizationCode,
                        "redirect_uri" to AuthZServer.REDIRECT_URL
                ))
                .authenticate(AuthZServer.CLIENT_ID, AuthZServer.CLIENT_SECRET)
                .responseObject(AccessTokenResponse.Deserializer())
            logger.debug("Got response: "+ response.responseMessage)
            val tokenResp = result.get()
            logger.debug("Got  Access Token: "+ tokenResp.access_token)
            logger.debug("Got  Refresh Token: "+ tokenResp.refresh_token)

            it.sessionAttribute("Token", tokenResp)      // Stored in the User Session

            it.render("prez.html", getModel(it,"Successfully got the Access Token, thanks!"))
        } catch (e :Exception) {
            logger.debug("Problem with the request ")
            it.render("prez.html", getModel(it,"Failed to get the Access Token! ${e.message}"))
        }

    }


    app.get("/delegaccess") {

        val accessToken = it.sessionAttribute<AccessTokenResponse>("Token")
        if (accessToken == null) {      // Hey!! It CAN be null!!
            logger.debug("No Access Token in the Session!!")
            it.redirect("/#no-access-token")
        } else {
            logger.info("Access Token from the session: " + accessToken.access_token )

            FuelManager.instance.basePath = RESOURCE_SERVER
            FuelManager.instance.baseHeaders = mapOf("Authorization" to "Bearer "+ accessToken.access_token )

            var msg: String
            try {
                val (request, response, result) = Fuel.get("/p/admin").responseString()
                logger.info("Result: " + result.get())
                logger.info("response: " + response.responseMessage)
                msg = "Successfully called Resource Server : "+ result.get()
            } catch (e: HttpException) {
                msg = "Failed to call Resource Server : "+ e.message
                logger.info(msg)
            }

            it.render("prez.html", getModel(it, msg))
        }
    }
}
