A client Web App with Javalin and Kotlin. Didactic, learning purpose only.

It is supposed to be used with Keycloak and another App acting as a Resource Server (RS)
to demo OAuth, but it can easily be adapted to other environments.

NOTE: there is another branch `no-scope` with a simpler project

## Use-cases



### No-Scope, No roles

Relates to the `no-scope` branch.

The Web App (_Client_ in OAuth) asks the user (you, the _Resource Owner_) if it can get an _Access Token_ from 
 the _Authorization Server_.
 
If you accept, you authenticate yourself on the _Authorization Server_ and consent to
 give the Web App a *delegation* to access your resources on the _Resource Server_.

Imagine that you have to investigate some strange behavior. You better have centralised log and correlations tools
to connect the HTTP requests to tokens, to WebApps, to users, to resources ... 
A user may have given a delegation years ago to some Web App ...  

Notes, in this implementation:
- The web App knows nothing about you, not even your name. Only that you are a valid user in the _Authorization Server_.
- The Web App stores the _Access and Refresh Tokens_ in the user session (server side). But it could store them in a database 
and use them when you are not connected as well, until you revoke the token.
- The Web App is registered in the _Authorization Server_ you can revoke its tokens and/or deregister the Web App completely (as an admin).
- The _Resource Server_ only checks that the Token is valid. If the WebApp was misbehaving or bugged, it could access
resources from other users with your token.
  - It could ask more info than only token's validity
  - It could directly "read" a non-opaque JWT.

Here is the big picture: ![overview.png](./overview.png)


### Scope, Roles, 2 legged

Relates to the `master` branch.

- The is no human involved. AKA 2 legged OAuth scenario or Client Credential flow
- The client gets its Access (and Refresh) Token with its credential (id/secret)
- The Token contains a Role (or scope, Authority ...)
- The Resource Server is able to open the JWT Token to validate and extract the role
- The Resource Server does Access Control based on the ClientApp's role
- (Keycloak always delivers JWT; no opaque token. A good thing for this demo)

Here is the big picture:  ![OAuth-ClientCred.png](./OAuth-ClientCred.png)



## Setup

The tested setup is as follow:
- Download and run locally a [Keycloak Authorization Server](https://www.keycloak.org/) (Note that this is dead easy, no need for Docker image).
- Clone my demo Resource Server: [rs-web-javalin](https://gitlab.com/OAuth-Demos/rs-web-javalin)

2 solutions to configure Keycloak (choose one):
- Manually:
  1. Create a "demo" [realm](https://www.keycloak.org/docs/3.2/server_admin/topics/realms/create.html)
  2. Create one or more users in the realm.
  2. Register the 2 Web App [OIDC Clients](https://www.keycloak.org/docs/3.2/server_admin/topics/clients/client-oidc.html) in this realm
  3. Configure them as [Confidential](https://www.keycloak.org/docs/3.2/server_admin/topics/clients/oidc/confidential.html) 
  4. Get the web app credentials *Client Secret* and change the WebApp code accordingly.
- Import:
  1. click "Add a Realm"
  2. Import the file: `src/test/resources/keycloak-realm-demo.json`
  3. It creates the "Demo" realm with clients and user "toto/toto" 

Overview of the Initialization: ![OAuth-Init.png](./OAuth-Init.png)

## Run

In a terminal, run `./gradlew run`

But the project is more about opening an IDE, read the code and run the main function.

Logs can be configured with SLF4J property file in the `src/main/resources/simplelogger.properties` and in the code when starting the server.

## Web App internals

I used (and learned) :
- [Javalin Web Framework](https://javalin.io/) (similar to Vertx or Spark, only way easier and less bloated)
- [Kotlin Language](https://KotlinLang.org/) (similar to Java and interoperable)
- [Fuel HTTP client](https://github.com/kittinunf/Fuel)
- [Gradle build tool](https://gradle.org/)
- [Thymeleaf HTML template](https://www.thymeleaf.org/) might not be the best choice for this project
- [Semantic-UI CSS](https://semantic-ui.com/)
- ...


- The JWT is managed with a library: [J-JWT](https://github.com/jwtk/jjwt)
- But the Json Web Keys (JWK) are managed manually. (J-JWT does not manage JWK yet. and it is not too difficult.)
- There are libraries that handle JWK (and JWT ...) [Nimbus JOSE+JWT](https://bitbucket.org/connect2id/nimbus-jose-jwt/wiki/Home)

## TODO

- The code could be vastly improved (remarks welcome)
- Use [Javalin Access manager](https://javalin.io/documentation#access-manager)
- Try to use / integrate [Simple OAuth 2.0 client written in Kotlin](https://github.com/mazine/oauth2-client-kotlin) looks really simple and good. 
- Upgrade to Javalin 2 (soon)
- Test if the provided a Keycloak configuration json file really work.

Second demo:
- Should use scopes
- OpenID Connect to get user's info

### Won't do

- See if [PAC4J](http://www.pac4j.org/docs/clients/oauth.html) is usable.
  - First tests are deceiving. Like many, it tries to hide the protocol to promote its own concepts. The result is confusing. 


## NOT for production

Again, this is definitively *NOT* for production. Typically, all the requests are done in the clear, to ease Wireshark inspection.

Recall that HTTP*S* MUST be used for all communications in OAuth.

But, I see so much confusion in OAuth usage, that a bit of simple demo seems necessary.

